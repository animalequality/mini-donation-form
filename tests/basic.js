import jsdom from 'jsdom';
import chai from 'chai';
import miniDonationFormInit from '../src/main.js';

const { assert } = chai,
      markup = `
<div
  id="test"
  class="form-mini foobar" data-title="Title" data-text="Here is the text" data-url="https://foo.bar/?s=param"
  data-amounts='[{"amount": 40, "selected": false}, {"amount": 80, "selected": true}]'
  data-button-label="Press it"
  data-url-profile="ea"
  data-button-class="btn btn--secondary align-center"
  data-other-label="100"
  data-other-value="100"
  data-currency="$"
  data-repeat-label="Monthly"
  data-repeat-preselected="false"
><noscript>Please enable Javascript in your browser in order to use this form.</noscript></div>
`,
      markup2 = `
<div class="everyaction-donation-form-mini"  data-url-profile="ea" data-url="https://perdu.com/donate/?box=true&amp;r=false" data-amounts="[{&quot;amount&quot;:&quot;10&quot;,&quot;selected&quot;:true}]" data-button-label="Press" data-button-class="btn btn--secondary align-center" data-other-label="100" data-other-value="100" data-currency="$" data-repeat-label="Monthly" data-repeat-preselected="true"></div>
`,
      markup_rn = `
<div class="bar" data-url-profile="raisenow" data-url="https://perdu.com/donate/?test=here" data-amounts="[{&quot;amount&quot;:&quot;60&quot;,&quot;selected&quot;:true}]" data-button-label="Press" data-button-class="btn btn--secondary align-center" data-other-label="100" data-other-value="100" data-currency="usd" data-repeat-label="Monthly"></div>
`;

function getA() {
  return Object.fromEntries(new URL(document.querySelector('a').getAttribute('href')).searchParams);
}


let my_jsdom = new jsdom.JSDOM(markup),
    window = my_jsdom.window.document.defaultView;
global.document = my_jsdom.window.document;

let f = miniDonationFormInit('.foobar');

assert.deepInclude(getA(), { s: 'param', r: 'false', am: '80' });
document.querySelector('.amount[data-value="80"]').click();
assert.deepInclude(getA(), { s: 'param', r: 'false', am: '80' });
document.querySelector('.amount[data-value="40"]').click();
assert.deepInclude(getA(), { s: 'param', r: 'false', am: '40' });
document.querySelector('input[type="checkbox"]').click();
assert.deepInclude(getA(), { s: 'param', r: 'true', SelectedFrequency: '4', recurringAm: '40' });

document.body.innerHTML = markup2;
f = miniDonationFormInit('.everyaction-donation-form-mini');
assert.deepInclude(getA(), { box: 'true', r: 'true', SelectedFrequency: '4', recurringAm: '10' });

document.body.innerHTML = markup_rn;
f = miniDonationFormInit('.bar');
assert.deepInclude(getA(), { test: 'here', 'rnw-currency': 'usd', 'rnw-amount': '60'});
document.querySelector('input[type="checkbox"]').click();
assert.deepInclude(getA(), { test: 'here', 'rnw-currency': 'usd', 'rnw-amount': '60',  'rnw-payment_type': 'recurring', 'rnw-recurring_interval': 'monthly'});



//console.log(document.body.innerHTML);
