import './main.scss';

/**
 * Renders a mini donation form. The forms contains multiple predefined amount buttons,
 * a input field for arbitrary amounts and a submit button. The mini form only passes
 * the amount parameter to the full donation form which then gets prefilled
 *
 * Currently this form supports iRaiser and Everyaction and Raisenow
 *
 * @param {HTMLElement} container The container in which the form should be rendered
 */
class MiniDonationForm {
  constructor(container) {
    this.container = container;
    this.url = new URL(container.dataset.url);

    const t = this;
    ({
      amount: t.amount = 0,
      amounts: t.amounts = '[]',
      buttonLabel: t.buttonLabel = '',
      buttonClass: t.buttonClass = '',
      otherLabel: t.otherLabel = '',
      title: t.title = '',
      text: t.text = '',
      repeatLabel: t.repeatLabel = '',
      repeatPreselected: t.repeat = 'false',
      otherValue: t.otherValue = null,
      target: t.target = 'self',
      maxAmount: t.maxAmount = null,
      maxAmountMessage: t.maxAmountMessage = null,
      minAmount: t.minAmount = null,
      minAmountMessage: t.minAmountMessage = null,
      utmSource: t.utmSource = null,
      utmMedium: t.utmMedium = null,
      utmCampaign: t.utmCampaign = null,
      utmTerm: t.utmTerm = null,
      utmContent: t.utmContent = null
    } = container.dataset);

    this.repeat = stringToBoolean(this.repeat);
    this.otherValue = !isNaN(this.otherValue) ? parseInt(this.otherValue) : null;
    this.maxAmount = !isNaN(this.maxAmount) ? parseInt(this.maxAmount) : null;
    this.minAmount = !isNaN(this.minAmount) ? parseInt(this.minAmount) : null;
    this.amounts = JSON.parse(this.amounts).map(({amount, selected}) => ({
      label: amount,
      value: amount,
      preselected: selected
    }));
  }

  init() {
    const titleHtml = this.title ? `<div class="mini-donation-form__headline">${this.title}</div>` : '';
    const textHtml = this.text ? `<p class="mini-donation-form__text">${this.text}</div>` : '';
    const preselectedAmount = this.amounts.filter(amount => amount.preselected)[0] || false;
    const repeatHtml = this.repeatLabel
      ? `<div class="mini-donation-form__repeat">
        <input type="checkbox" id="mini-donation-form-repeat" ${this.repeat ? 'checked' : ''} />
        <label for="mini-donation-form-repeat">
          ${this.repeatLabel}
        </label>
      </div>`
      : '';
    const maxAmountMessage = this.maxAmount && this.maxAmountMessage ? `<p class="mini-donation-form__max-amount-error" style="display: none;">${this.maxAmountMessage}</p>` : '';
    const minAmountMessage = this.minAmount && this.minAmountMessage ? `<p class="mini-donation-form__min-amount-error" style="display: none;">${this.minAmountMessage}</p>` : '';

    const amountsHtml = this.amounts.map(amount => `<div class="amount" data-value="${amount.value}">
        ${this.getAmountWithCurrency(amount.label)}
      </div>`).join('');

    const template = `
        <form>
          ${titleHtml}
          ${textHtml}
          <div class="mini-donation-form__interaction ${this.amounts ? '' : 'mini-donation-form__interaction--no-amounts'}">
            <div class="mini-donation-form__amounts">
              ${amountsHtml}
              <div class="amount amount--other">
                ${this.prependCurrency ? this.currency : ''}<input id="other-amount-val" type="number" min="${this.minAmount ? this.minAmount : 0}" ${this.maxAmount ? `max=${this.maxAmount}` : ''} step="0.01" placeholder="${this.otherLabel}" ${this.prependCurrency ? 'class="prepend-currency"' : ''} />${!this.prependCurrency ? this.currency : ''}
              </div>
            </div>
            ${repeatHtml}
            ${maxAmountMessage}
            ${minAmountMessage}
            <a href="" class="btn-submit ${this.buttonClass}" ${this.target === 'blank' ? 'target="_blank"' : ''} rel="norefferer noopener">
              ${this.buttonLabel}
            </a>
          </div>
        </form>
      `;

    this.container.classList.add('mini-donation-form');
    this.container.innerHTML = template;

    this.amountNodes = this.container.querySelectorAll('.amount:not(.amount--other)');
    this.amountOtherNode = this.container.querySelector('.amount--other');
    this.amountOtherInputNode = this.container.querySelector('#other-amount-val');
    this.buttonNode = this.container.querySelector('a');
    this.repeatNode = this.container.querySelector('#mini-donation-form-repeat');
    this.maxAmountMessageNode = this.container.querySelector('.mini-donation-form__max-amount-error');
    this.minAmountMessageNode = this.container.querySelector('.mini-donation-form__min-amount-error');

    this.amountNodes.forEach(amount => {
      amount.addEventListener('click', () => {
        this.selectAmount(amount);
      });
    });

    this.amountOtherNode.addEventListener('click', () => {
      this.unselectAmounts();
      this.amountOtherInputNode.focus();
      this.amountOtherNode.classList.add('selected');
      this.updateAmount(null);
    });

    this.amountOtherNode.addEventListener('keyup', event => {
      this.updateAmount(event.target.value);
    });

    this.repeatNode && this.repeatNode.addEventListener('change', event => {
      this.repeat = event.target.checked;
      this.updateButtonHref();
    });

    if (preselectedAmount) {
      this.selectAmount(this.container.querySelector(`.amount[data-value="${preselectedAmount.value}"]`));
    } else if (this.otherValue) {
      this.amountOtherNode.querySelector('input').setAttribute('placeholder', this.otherValue);
      this.updateAmount(this.otherValue);
    } else {
      this.selectAmount(this.container.querySelector('.amount:first-child'));
    }

    this.updateButtonHref();
  }

  addUtmParametersToUrl() {
    const currentUrlSearchParams = new URLSearchParams(window.location.search);
    const utmParameters = [
      {key: 'utm_source', name: 'utmSource'},
      {key: 'utm_medium', name: 'utmMedium'},
      {key: 'utm_campaign', name: 'utmCampaign'},
      {key: 'utm_content', name: 'utmContent'},
      {key: 'utm_term', name: 'utmTerm'}
    ];

    utmParameters.forEach(({key, name}) => {
      this.url.searchParams.delete(key);

      if (this[name] || currentUrlSearchParams.get(key)) {
        this.url.searchParams.append(key, this[name] ? this[name] : currentUrlSearchParams.get(key));
      }
    });
  }

  /**
     * Takes the given amount and adds the currency symbol
     * @param {number} amount
     * @returns {string}
     */
  getAmountWithCurrency(amount) {
    return `${this.prependCurrency ? this.currency : ''}${amount}${this.prependCurrency ? '' : this.currency}`;
  }

  /**
     * Unselects the given amount elements
     * @param {HTMLElement[]} amounts
     */
  unselectAmounts() {
    this.amountNodes.forEach(amount => {
      amount.classList.remove('selected');
    });
    this.amountOtherNode.classList.remove('selected');
  }

  /**
     * Selects the given amount
     * @param {HTMLElement} amountToSelect
     */
  selectAmount(amountToSelect) {
    this.unselectAmounts();
    amountToSelect.classList.add('selected');
    this.updateAmount(amountToSelect.dataset.value);
  }

  /**
     * Updates the button HREF with the newly selected amount
     * @param {number} newAmount
     */
  updateAmount(newAmount) {
    if (isNaN(parseInt(newAmount))) {
      this.amount = null;
      this.updateButtonHref(false);
      return;
    }

    this.amount = newAmount * this.constructor.MULTIPLIER;

    if (this.maxAmount && this.maxAmountMessageNode) {
      this.maxAmountMessageNode.style.display = newAmount > this.maxAmount ? 'block' : 'none';
    }

    if (this.minAmount && this.minAmountMessageNode) {
      this.minAmountMessageNode.style.display = newAmount < this.minAmount ? 'block' : 'none';
    }

    this.updateButtonHref(this.maxAmount && newAmount > this.maxAmount && this.minAmount && newAmount < this.minAmount);
  }

  /**
     * Updates the button HREF with the newly selected amount
     * @param {boolean} exceedingMaxAmount
     */
  updateButtonHref(exceedingMaxAmount) {
    this.addUtmParametersToUrl();
    this.buttonNode.setAttribute('href', exceedingMaxAmount ? '#' : this.toUrl());
  }

  /**
     * Get the URL as a string
     * @returns {string}
     */
  toUrl() {
    return this.url.toString();
  }
};

/**
 * https://docs.ngpvan.com/reference/query-strings
 */
class EveryActionProfile extends MiniDonationForm {
  constructor(...args) {
    super(...args);
    this.amountParameterName = 'am';
    this.amountParameterRecurringName = 'recurringAm';
    this.repeatParameterName = 'r';
    /*
      0 • one-time
      1 • weekly
      2 • every two weeks
      3 • every four weeks
      4 • monthly
      5 • quarterly
      6 • yearly
      7 • twice a year
    */
    this.frequencyParameterName = 'SelectedFrequency';
    this.currency = '$';
    this.prependCurrency = true;
  }

  static get MULTIPLIER() { return 1; }

  toUrl() {
    this.url.searchParams.set(this.frequencyParameterName, this.repeat ? 4 : 0);
    this.url.searchParams.set(this.repeatParameterName, this.repeat.toString());
    if (this.repeat) {
      this.url.searchParams.delete(this.amountParameterName);
      this.url.searchParams.set(this.amountParameterRecurringName, this.amount);
    } else {
      this.url.searchParams.set(this.amountParameterName, this.amount);
      this.url.searchParams.delete(this.amountParameterRecurringName);
    }

    return this.url.toString();
  };
}

class FindockProfile extends MiniDonationForm {
  constructor(...args) {
    super(...args);
    this.amountParameterName = 'amount';
    this.repeatParameterName = 'is_recurring';
    this.currency = '€';
    this.prependCurrency = false;
  }

  static get MULTIPLIER() { return 1; }

  toUrl() {
    this.url.searchParams.set(this.amountParameterName, this.amount);
    this.url.searchParams.set(this.repeatParameterName, this.repeat);
    return this.url.toString();
  }
};

class IRaiserProfile extends MiniDonationForm {
  constructor(...args) {
    super(...args);
    this.amountParameterName = 'amount';
    this.repeatParameterName = 'frequency';
    this.currency = '€';
    this.prependCurrency = false;
  }

  static get MULTIPLIER() { return 100; }

  toUrl() {
    this.url.searchParams.set(this.amountParameterName, this.amount);

    if (this.repeat) {
      this.url.searchParams.set(this.repeatParameterName, 'regular');
    } else {
      this.url.searchParams.delete(this.repeatParameterName);
    }

    return this.url.toString();
  }
};

/**
 * https://support.raisenow.com/hc/en-us/articles/360011882157-Prefill-fields-via-URL-Parameters
 */
class RaiseNowProfile extends MiniDonationForm {
  constructor(...args) {
    super(...args);
    this.amountParameterName = 'rnw-amount';
    this.repeatParameterName = 'rnw-payment_type'; // == recurring
    /*
      monthly
      quarterly
      yearly
      semestral
    */
    this.frequencyParameterName = 'rnw-recurring_interval';
    // ToDo 'rnw-purpose', 'rnw-payment_method'

    this.currency = '€';
    this.prependCurrency = false;
  }

  static get MULTIPLIER() { return 1; }

  /**
   * Updates the button HREF with the newly selected amount
   * @param {Object} newAmount
   */
  updateAmount(newAmount) {
    super.updateAmount(newAmount);
  }

  toUrl() {
    this.url.searchParams.set(this.amountParameterName, this.amount);

    if (this.repeat) {
      this.url.searchParams.set(this.repeatParameterName, 'recurring');
      this.url.searchParams.set(this.frequencyParameterName, 'monthly');
    } else {
      this.url.searchParams.delete(this.repeatParameterName);
      this.url.searchParams.delete(this.frequencyParameterName);
    }

    return this.url.toString();
  }
};

/**
 * https://stackoverflow.com/a/1414175/5441588
 */
function stringToBoolean(string) {
  if (!string) {
    return false;
  } else if (string === true || string === false) {
    return string;
  }

  switch (string.toLowerCase().trim()) {
    case 'true':
    case 'yes':
    case '1':
      return true;
    case 'false':
    case 'no':
    case '0':
    case null:
      return false;
    default:
      return Boolean(string);
  }
}

export default selector => {
  const sections = document.querySelectorAll(selector);
  const forms = [];
  const profiles = {
    everyaction: EveryActionProfile,
    iraiser: IRaiserProfile,
    raisenow: RaiseNowProfile,
    findock: FindockProfile
  };

  for (const section of sections) {
    const profile = section.dataset.urlProfile;

    if (!profile || !profiles[profile]) {
      throw Error('No profile');
    }

    const form = new profiles[profile](section);
    form.init();
    forms.push(form);
  }

  return forms;
};
